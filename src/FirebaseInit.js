import firebase from 'firebase/app';
import 'firebase/messaging';

export const VAPID_KEY = 'BGC1nvU3CCdkF09pk9_dIAhcOJVP47igvQyvriXK8FbLqnHRF-q4g13bT0p5Eatw-QQYHtLaZ4AL0SO7Px74u8o';

export const FIREBASE_CONFIG = {
  apiKey: "AIzaSyB2pwFvFiUVPdV2oG0k41wnq5jrk46usVM",
  authDomain: "dsme-touchbiz-sit.firebaseapp.com",
  projectId: "dsme-touchbiz-sit",
  storageBucket: "dsme-touchbiz-sit.appspot.com",
  messagingSenderId: "677221810839",
  appId: "1:677221810839:web:99afc1876053b59e9c5fd2"
};

firebase.initializeApp(FIREBASE_CONFIG);

const messaging = firebase.messaging();

export const getToken = async () => {
  try {
    return await messaging.getToken({ vapidKey: VAPID_KEY });
  } catch (error) {
    console.log("An error occurred while retrieving token. ", error);
    return error;
  }
};

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
