import { useEffect } from "react";
import { getToken } from "../../FirebaseInit";

const Notifications = () => {
  const setupToken = async () =>{
    const data = await getToken();
    console.log('Token Is: ', data);
  }

  useEffect(() => {
    setupToken();
  }, []);

  return null;
};

Notifications.propTypes = {};

export default Notifications;
