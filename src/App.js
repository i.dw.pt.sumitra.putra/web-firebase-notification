import "./App.css";
import Fader from "./components/Fader";

import React, { useState } from "react";
import { onMessageListener } from "./FirebaseInit";
import Notifications from "./components/Notifications/Notifications";
import ReactNotificationComponent from "./components/Notifications/ReactNotification";

function App() {
  const [notification, setNotification] = useState({ title: "", body: "" });

  onMessageListener()
    .then((payload) => {
      setNotification({
        title: payload.notification.title,
        body: payload.notification.body,
      });
      console.log('APP_payload', payload);
    })
    .catch((err) => console.log("failed: ", err));

  return (
    <div className="App">
      <Notifications />
      <ReactNotificationComponent
        title={notification.title}
        body={notification.body}
      />
      <Fader text="Hello React" />
    </div>
  );
}

export default App;
