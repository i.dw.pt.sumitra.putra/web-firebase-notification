/* eslint-disable no-undef */
importScripts("https://www.gstatic.com/firebasejs/8.8.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.8.0/firebase-messaging.js");

const FIREBASE_CONFIG = {
  apiKey: "AIzaSyB2pwFvFiUVPdV2oG0k41wnq5jrk46usVM",
  authDomain: "dsme-touchbiz-sit.firebaseapp.com",
  projectId: "dsme-touchbiz-sit",
  storageBucket: "dsme-touchbiz-sit.appspot.com",
  messagingSenderId: "677221810839",
  appId: "1:677221810839:web:99afc1876053b59e9c5fd2"
};

firebase.initializeApp(FIREBASE_CONFIG);

const messaging = firebase.messaging();

messaging.onBackgroundMessage( (payload) => {
  console.log("Received background message ", payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: "/logo192.png",
  };

  // eslint-disable-next-line no-restricted-globals
  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});
